#!/usr/bin/env node

const AWS = require('aws-sdk');
const fs = require('fs');
const path = require( 'path' );
const s3 = new AWS.S3();
const bucket = 'io-boomfunc-bmp';

const readFile = (filename, callback) => {
fs.readFile(filename, (err, buffer) => {
	if (!err) {
		callback(buffer);
	} else {
		console.error(err);
		process.exit(1);
	}
});
};

const getFileList = (dir, filelist) => {
let files = fs.readdirSync(dir)
filelist = filelist || []

files.forEach( file => {
	if (fs.statSync(dir + '/' + file).isDirectory()) {
	filelist = getFileList(dir + '/' + file, filelist)
	} else {
	filelist.push(dir + '/' + file)
	}
})
return filelist
}

const dist = path.resolve('./dist')
const project = require( path.resolve( './package.json')  )

const getMimeType = filepath => {
	if ( filepath.indexOf('.js') >= 0 ) {
		return 'text/javascript'
	} else if (filepath.indexOf('.css') >= 0 ) {
		return 'text/css'
	} else {
		return 'text/plain'
	}
}

getFileList( dist ).forEach( filePath => {
	readFile( filePath , buffer => {
		var filePathAbs = filePath.replace( dist+'/', '' )
		let realPath = `${project.name}/${project.version}/${filePathAbs}`
		console.log('\x1b[33mUploading ', realPath, '...' )

		let params = {
		Bucket: bucket,
		Key: realPath,
		Body: buffer,
		ACL: "public-read",
		ContentType: getMimeType(filePath),
		CacheControl: 'no-cache,must-revalidate',
		// Metadata: {
		//   "Access-Control-Allow-Origin": "*",
		//   "Access-Control-Allow-Methods": "GET, HEAD",
		// 	"Access-Control-Allow-Credentials": 'false',
		// 	'Cache-Control': 'no-cache,must-revalidate'
		// }
		//  --cache-control 'no-cache,must-revalidate'
		};
		let options = { partSize: 10 * 1024 * 1024, queueSize: 1 };
		s3.upload(params, options, (err, data) => {
		if (!err)
			console.log( 'Success upload ', filePath, 'to', data );
		else {
			console.error( '\x1b[31mUPLOAD ERROR: ' )
			console.error( '\x1b[37mMessage: ', err.message )
			console.error( 'Origin:  ', err.originalError.message )
			process.exit(1)
		}
		});

	});

})
