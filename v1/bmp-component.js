#!/usr/bin/env node
console.log( "\x1b[33m", 'Loading requirements...' );
const commandLineArgs = require( 'command-line-args' );
const getUsage = require( 'command-line-usage' );

const { spawn } = require('child_process');


const optionDefinitions = [
  { name: 'help', alias: 'h', type: Boolean, description: 'Display the usage guide.' },
  { name: 'watch', alias: 'w', type: Boolean, description: 'Watch for changes in styles and scripts' },
	{ name: 'build', alias: 'b', type: Boolean, description: 'Build src to dist' },
	{ name: 'es6', alias: 'e', type: Boolean, description: 'Use es6 syntax in bundler' },
  { name: 'rollup', alias: 'r', type: Boolean, description: 'Use rollup library in task' }
];

const usage = getUsage([
  {
    header: 'Boompack component cli tool',
    content: 'Use for build and watch src'
  },
  {
    header: 'Options',
    optionList: optionDefinitions
  }
]);

const options = commandLineArgs( optionDefinitions )
options.version = options.version  || '1'
let subprocess
let processOpts = []

if ( options.help || Object.keys( options ).length <= 0 ) {
  console.log( usage )
  process.exit( 0 )
} else if ( options.watch ) {
  // spawn watch process
  processOpts.push( `/srv/bmp-component/watch.js` )

} else if ( options.build ) {
  // spawn build process
  processOpts.push( `/srv/bmp-component/build.js` )

}
if ( options.rollup ) processOpts.push( '--rollup=true' )
if ( options.es6 ) processOpts.push( '--es6=true' )

let green = '\x1b[32m';
subprocess = spawn( 'node', processOpts)
console.log(green, 'New process spawned: ',subprocess.pid )
subprocess.unref()
subprocess.stdout.on('data', data => {
  console.log(`${data}`)
})
subprocess.stderr.on('data', data => {
  console.log(` ${data}`)
})

// process.exit( 0 );
