const fs = require('fs')
const path = require('path')
/**
 * Method trying to find files in directory with extensions and running "done" function when search is ended or throw some erorr.
 * @module findFiles
 * @param {string} dir - directory where module will search files
 * @param {string} findExts - extensions of files that need to be found
 * @param {string} done - callback, called after find all files. Passing (error, results) params
 */
const findFiles = (dir, findExts, result) => {
  const list = fs.readdirSync( dir )
  result = result || []
  
  list.forEach( file => {
    file = path.resolve(dir, file);
    // console.log( file )
    const stat = fs.statSync( file )
    if ( stat && stat.isDirectory() ) 
      return findFiles( file, findExts, result )
    
    let extension = path.parse(file).ext
    
    if ( findExts.indexOf( extension ) > -1 )
      result.push( file );
  })
  return result

}
exports.default = findFiles