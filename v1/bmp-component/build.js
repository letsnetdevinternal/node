
const Transform = require( './transform' )
const findFiles = require( './_find-files' ).default

const path = require('path')
let fs = require('fs-extra')


class Builder extends Transform {

  _js() {
    // build js

    // search js files
    const source = findFiles( `${this.source}/scripts/`, ['.js'] )

    return this.buildJS( source ).catch( console.log )

  }

  _sass () {

    const source =
      findFiles( `${this.source}/styles/`, ['.sass', '.scss'] )
      .filter( filePath => {
        return filePath && !filePath.split( '/' ).find(
          segment => segment.indexOf('_') === 0
        )
      })

    let promises = source.map( originSrc => {
      const distSrc = originSrc
        .replace(
          path.resolve(this.source), // change folder source to dist
          path.resolve(this.dist)
        )
        .replace(/\.\w+$/, '') + '.css';

      const promise = this.buildSass( originSrc, distSrc );
      promise
        .then( res =>
          console.log(
            this.color.success, '- Sass completed ', this.color.white, res.join('\n')
          )
        )
        .catch( console.log )
      return promise
    })

    return Promise.all( promises )
  }



  start() {

    fs.copySync( this.source + '/assets/', `${this.dist}/assets/` )

    return Promise.all( [this._sass(), this._js()] )
      .then( res => {

        let files = {};
        ['styles', 'scripts'].forEach( folder => {
          files[ folder ] =
            this
              .getFileList( `${this.dist}/${folder}` )
              .map( path => path.replace(`${this.dist}/`, '') );
        });

        fs.outputFileSync( `${ this.dist }/files.json`, JSON.stringify( files ));
      })
      .catch( console.log );

  }


}

const builder = new Builder(
  path.resolve('./dist'),
  path.resolve('./src')
);
builder.compressed = true;
builder.useRollup = process.argv.find( arg => arg == '--rollup=true' ) != undefined
builder.useBabel = !(process.argv.find( arg => arg == '--es6=true' ) != undefined)
builder.start()
  .then( _ => { process.exit(0) })
  .catch( _ => { process.exit(1) })
