
// const babel = require('@babel/core')
const babel = require( 'babel-core' )
const fs = require('fs-extra')
const uglify = require("uglify-es")
const path = require('path')

let sass = require( 'node-sass' );


const fatalError = (msg) => {
  console.log('FATAL ERROR. Failed to build project:');
  console.log(msg);
  process.exit(0)
}

const _getFileName = (path) => {
  return path.replace(/.*\/([a-z0-9-_]+\.[a-z]+)$/, '$1');
}
'use strict';


/**
 *
 * @constructor
 * @param {string} folder - Required. Path to temporary folder which the project will be built
 * @param {string} source - Required. Path to source folder to be watched
 * @example
 */
class Transform {

  /**
   * @constructor
   */
  constructor(dist, source) {
    this.color = {
      danger: "\x1b[31m",
      success: "\x1b[32m",
      warning: "\x1b[33m",
      info: "\x1b[34m",
      white: "\x1b[37m"
    };
    this.useRollup = false

    // clear folder
    if ( fs.existsSync( dist ) )
      fs.removeSync( dist );

    fs.mkdirSync( dist );

    this.dist = dist;
    this.source = source;
  }

  /**
   * Build JS by passed source if not
   * @param {(Array|String)} source source filename(s)
   * @return {Promise} Promise
   * @example
   * buildJS( [
   *  '/src/app/module1.js',
   *  '/src/app/module2.js'
   * ] );
   * buildJS('/src/app/module3.js');
   */
  buildJS( source ) {

    return new Promise( (resolve, reject) => {
      if ( !Array.isArray(source) )
        source = [source];

      /**
       * @type {int}
       */
      let loaded = [];
      let completed = (newFilePath, skipMinify = false) => {

				if ( this.compressed && !skipMinify) {
					const code = this._minifyJS( fs.readFileSync(newFilePath).toString() )
					fs.outputFileSync( newFilePath, code )
				}

        loaded.push( newFilePath )
        if ( loaded.length == source.length ) resolve( loaded )
      }

      source.forEach( filepath => {
        /**
         * Filename of source file path
         * @const {string}
         */
				if ( filepath.includes('/libs/') ) {
					const distPath = path.resolve( filepath )
						.replace(
							path.resolve(this.source),
							path.resolve(this.dist)
						);
					fs.outputFileSync( distPath, fs.readFileSync(filepath) )
					completed(distPath, true)
				} else if ( !this.useBabel ) { // only rollup
          this._rollup( filepath ).then( distPath => {
						completed()
					}).catch( reject )
        } else if ( !this.useRollup ) { // only babel
          this._babel( filepath ).then( completed ).catch( reject )
				} else { // babel + rollup
          this._rollup( filepath ).then( destPath => {
						this._babel( destPath ).then( completed ).catch( reject )
          }).catch( reject )
        }

      })
    })

  }

  _rollup( filePath ) {
    return new Promise( (resolve, reject) => {

      const destPath = path
        .resolve( filePath )
        .replace(
          path.resolve(this.source),
          path.resolve(this.dist)
        )

			const amd = require( 'rollup-plugin-amd' )
      let options = {
        entry: filePath,
        dest: destPath,
        format: 'iife',
        moduleName: 'component',
        plugins: [
          amd(),
        ]
      }
      // console.log( this.color.success, filePath )
      fs.outputFileSync( destPath, '' )
      // console.log( `\x1b[33m`, destPath )

      require( 'rollup' )
        .rollup( options )
        .then( result => {
          result
            .write( options )
            .then( res => {
              console.log( this.color.success, '- Rollup completed: ', this.color.white, destPath )

              resolve( destPath )
            }).catch( reject )
        }).catch( reject )
    })
  }


  /**
   * Build Sass by passed source
   * @param {(Array|String)} source source file path
   * @param {(Array|String)} output output file path
   * @return {Promise} Promise
   * @example
   * buildSass( './dist/styles/styles.sass', './dist/styles/styles.css' )
   */
  buildSass( source, output ) {

    let fileName = _getFileName( source );
    this.createFoldersTree( source.replace( fileName, '' ) );
    fs.outputFileSync( output, '' );
    // Create promise
    const promise = new Promise( (resolve, reject) => {
      // Transform sass from file to single css
      sass.render({
        file: source,
        outputStyle: this.compressed ? 'compressed' : false
      }, (err, res) => {
        if ( !err ) {
          // write rendered sass to file
          fs.writeFile(output , res.css, err => {
            if ( !err ) resolve( [output] )
            else reject( err )

          })
        } else {
          reject(err);
        }

      })
    })
    return promise;
  }


  _babel( filepath ) {
    const bOptions = {
      comments: false,
      plugins: [ require.resolve('babel-plugin-transform-es2015-modules-amd') ],
      presets:  [
        // [require.resolve('@babel/preset-env'), {
        [require.resolve('babel-preset-env'), {
          // TODO: replace it to package.json (field "browserslist")
          targets: { browsers: ["ie >= 11"] },
          useBuiltIns: false
        }]
      ]
		}

    return new Promise( (resolve, reject) => {
      babel.transformFile( filepath , bOptions, (err, res) => {
				if ( err ) return reject( err )

        // write builded file
        // replace in file path source folder to dist folder
        const distPath = path
          .resolve( filepath )
          .replace(
            path.resolve(this.source),
            path.resolve(this.dist)
          );

        fs.outputFileSync( distPath, res.code );
        console.log( this.color.success, '- Babel completed: ', this.color.white, distPath );
        resolve( distPath );

      });
    });

  }


  _minifyJS(code) {
    // minify file
    let minify = uglify.minify(code);
    if ( minify.error ) {
      console.log( this.color.danger, '- Error minify JS', minify );
      return false;
    }

    return minify.code;
  }


  createFoldersTree( path ) {
    // create working tree
    let segments = '';
    path.split( '/' ).forEach( segment => {
      // read all segments in path
      segments += '/' + segment;
      if ( !fs.existsSync( segments ) ) {
        // create dir if not exists
        fs.mkdirSync( segments );
      }
    });
  }


  getFileList(dir, filelist) {

    let files = fs.readdirSync(dir);
    filelist = filelist || [];

    files.forEach( file => {
      if (fs.statSync(dir + '/' + file).isDirectory()) {
        filelist = this.getFileList(dir + '/' + file, filelist);
      } else {
        filelist.push(dir + '/' + file);
      }
    });
    return filelist;
  };

}

module.exports = Transform;
