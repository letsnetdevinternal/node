const chokidar = require('chokidar');
const path = require('path');
const fs = require( 'fs-extra' );
const white = '\x1b[37m';

console.log('Starting watcher...');
const waitChanges = () => {
  console.log('\x1b[33mWaiting for changes...'+white);
};
const fatalError = (msg) => {
  console.log('\x1b[31mFATAL ERROR. Failed to initialize project: '+white);
  console.log(msg);
  process.exit(0);
}

const Transform = require( './transform' );


/**
 * Main watch class. Can watch to the files in source folder and build project to the temporary folder.<br/>
 * It detecting changes in *.sass, *.scss, *.js, *.html files.<br/>
 * Should be runned as CLI Interface by npm manager: "npm run watch -- ns=explore common=false".<br/>
 * Can parse CLI Arguments ns, common
 * @constructor
 * @param {string} folder - Required. Path to temporary folder which the project will be built
 * @param {string} source - Required. Path to source folder to be watched
 * @example
 * bmp-component --watch
 */
class Watch extends Transform {

  /**
   * Method should be runned when file is changed
   * @method
   * @param {string} file_path - Path to file that was changed
   */
  _onChange(file_path) {

    // const fileHash = md5File.sync( file_path );
    // if ( fileHash === this.fileHash[file_path] ) {
      // console.log( this.color.info, `File content is identical, aborting...`, this.color.white, file_path );
      // waitChanges();
    // } else {

      // this.fileHash[file_path] = fileHash;

      let extension = file_path.toLowerCase().replace(/^.*\.(\w+)$/, '$1');
      switch (extension) {
        case "sass":
        case "scss":
        case "css":
          // search sass file in regestry of root files

          var promises = Object
            .keys( this.sassRegestry )
            .map( buildFile => {

              if ( this.sassRegestry[buildFile].indexOf( file_path ) > -1 || buildFile == file_path ) {
                let distFilePath = buildFile
                  .replace(
                    path.resolve(this.source),
                    path.resolve(this.dist)
                  )
                  .replace(
                    /^(.*)\.\w+$/,
                    '$1.css'
                  )

                let promise = this.buildSass( buildFile, distFilePath )
                promise.then( res => {
                  console.log(this.color.success, '- Sass completed ', this.color.white, distFilePath)
                }).catch( err => {
                  console.log(this.color.danger, '- Sass error ', this.color.white, err)
                });
                return promise
              }
            })
          return Promise.all( promises )
          break;
        case "js":

          return this.buildJS( file_path )
          break;
        default:
          // copy file to dist
          const copy_path = file_path.replace(
            path.resolve(this.source),
            path.resolve(this.dist)
          )
          const old_file = fs.readFileSync(file_path);
          fs.outputFileSync( copy_path, old_file )

          console.log(this.color.success, 'Copied from ', file_path, 'to', copy_path);
          break;

      }
    // }
  }


  /**
   * Extract all @import strings from file path
   * @param {String} filePath file path that will be read and extract all imports from it
   * @returns {Array} Import strings in file
   */
  _getSassImports( filePath ) {
    let content = fs.readFileSync( filePath, { encoding: 'utf8' } );

    let regex = /^\@import\s+.([\/\w-.]+)./gm;
    let match = regex.exec( content );
    let imports = [];
    while (match != null) {
      // get all matches
      imports.push( match[1] )
      match = regex.exec( content );
    }
    return imports;
  }


  _registerStyleImports( filePath ) {

    // check file for "_" prefix
    let isExclude = filePath.split( '/' ).find( segment => segment.indexOf( '_' ) === 0 ) || false

    if ( !isExclude ) {
      // clear regestry
      this.sassRegestry[filePath] = []

      // get all imports of file
      const imports = this._getSassImports( filePath )
      const folder = path.parse( filePath ).dir

      imports.forEach( importPath => {
        const {dir, name} = path.parse( `${folder}/${importPath}` )
        let filePaths = [
          `${dir}/${name}`, // '@import "dir/file.sass"'
          `${dir}/_${name}`, // '@import "dir/_file.sass"'
        ];
        filePaths.forEach( fpath => {
          filePaths = filePaths.concat(
            // file can be in 3 extentions
            ['.sass', '.scss', '.css'].map( ext => fpath + ext )
          )
        })

        const filePathResolved =
          filePaths
            .map( f => path.resolve(f) )
            .find( f => fs.existsSync(f) )

        // add deep file
        if ( filePathResolved )
          this.sassRegestry[filePath].push( filePathResolved )
        else if ( this.verbose )
          console.log( this.color.error, 'Error in sass file', filePath ,' \n import not found ', importPath)
      });
    }
  }


  scanDir() {
    fs
      .copy( this.source + '/assets/', this.dist + '/assets/' )
      .then( _ => { console.log( this.color.warning, 'Copy assets complete.' ) } )
      .catch( err => { console.log( this.color.danger, 'Copy assets error', err ) } )

    this.sassRegestry = {}
    this.fileHash = {}
    this.verbose = true
    this.compressed = false

    const chokiOpts = {
      persistent: true,
      usePolling: true,
      awaitWriteFinish: {
        stabilityThreshold: 500
      }
    };

    const watcher = chokidar.watch(
      [
        `${this.source}/styles`,
        `${this.source}/scripts`
      ],
      chokiOpts
    );

    watcher
      .on( 'add', path => {
        if ( this.verbose ) console.log(this.color.success, `- Watch: File added`, path)

        if ( /\.sass$/.test( path ) ) // add sass imports to regestry
          this._registerStyleImports( path )

        // first build for file
        let promise = this._onChange(path)
        if (promise) promise.catch( e => { console.log( 'Prebuild error', e  ) } )

      } )
      .on('change', path => {
        if ( this.verbose ) console.log(this.color.warning, `- Watch: File changed`, this.color.white, path);

        if ( /.sass$/.test( path ) ) // add sass imports to regestry
          this._registerStyleImports( path )

        this._onChange( path )
          .then( waitChanges )
          .catch( err => {
            console.log(this.color.danger, '- Watch error ', err)
          })

      }).on('ready', _ => console.log(this.color.warning, 'Styles scan complete. Ready for SASS changes'))

  }


}


const watcher = new Watch(
  path.resolve('./dist'),
  path.resolve('./src')
);

watcher.useBabel = !(process.argv.find( arg => arg == '--es6=true' ) != undefined)
watcher.useRollup = process.argv.find( arg => arg == '--rollup=true' ) != undefined
watcher.scanDir();
