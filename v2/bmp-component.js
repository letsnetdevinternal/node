#!/usr/bin/env node
console.log( 'Loading requirements...' );
const commandLineArgs = require( 'command-line-args' );
const getUsage = require( 'command-line-usage' );

const { spawn } = require('child_process');

const optionDefinitions = [
  { name: 'help', alias: 'h', type: Boolean, description: 'Display the usage guide.' },
  { name: 'watch', alias: 'w', type: Boolean, description: 'Watch for changes in styles and scripts' },
  { name: 'reload', alias: 'r', type: Boolean, description: 'Run browser autoreload socket' },
  { name: 'build', alias: 'b', type: Boolean, description: 'Build src to dist' },
  { name: 'babel', alias: 't', type: Boolean, description: 'Use babel transpiler in bundle' },
  { name: 'minify', alias: 'm', type: Boolean, description: 'Minify code in dist' },
  { name: 'env', alias: 'e', type: String, description: 'Process enviroment. development|production' },
  { name: 'project', alias: 'p', type: String, description: 'Project name' }
];

const usage = getUsage([
  {
    header: 'Boompack component cli tool',
    content: 'Use for build and watch src'
  },
  {
    header: 'Options',
    optionList: optionDefinitions
  }
]);

const options = commandLineArgs( optionDefinitions )
let subprocess
let processOpts = []

if ( options.help || Object.keys( options ).length <= 0 ) {
  console.log( usage )
  process.exit( 0 )
} else if ( options.watch ) {
  // spawn watch process
  processOpts.push( `/srv/bmp-component/watch`, options.project )

} else if ( options.build ) {
  // spawn build process
  processOpts.push( `/srv/bmp-component/build`, options.project )

} else if ( options.reload ) {
	// spawn reload process
	processOpts.push( `/srv/bmp-component/serve/socket` )

} else if ( options.minify ) {
	// spawn minify process
	processOpts.push( `/srv/bmp-component/minify` )

}

if ( options.env ) processOpts.push( `--env=${options.env || 'development'}` )
if ( options.babel ) processOpts.push( `--babel` )

subprocess = spawn( 'node', ['--experimental-modules', ...processOpts])
console.log( 'New process spawned:', subprocess.pid )
subprocess.unref()
subprocess.stdout.on('data', data => {
  console.log(`${data}`)
})
subprocess.stderr.on('data', data => {
  console.log(` ${data}`)
})

// process.exit( 0 );
