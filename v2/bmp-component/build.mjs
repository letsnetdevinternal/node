
/** Core */
import path from  'path'
import request from  'request'
import fs from  'fs-extra'

/** Rollup dependencies */
import rollup from 'rollup'
import amdToES6 from  '@buxlabs/amd-to-es6'
import { sassToJs } from './wrappers/sass-to-js'
import babel from './wrappers/babel'

import babelplugin from 'rollup-plugin-babel'
import noderesolver from 'rollup-plugin-node-resolve';
import virtual from 'rollup-plugin-virtual'
import commonjs from 'rollup-plugin-commonjs'

import { PKG_JSON, pathByScript } from './helpers/path-resolver'
import { log } from './helpers/logger';

const download = uri => {
	return new Promise( resolve => {
		request
			.get({ uri }, (err, res, data) => {
				if ( err ) {
					console.warn( `Fail: cannot get "${ uri }",`,err )
				} else {
					resolve( data )
					console.log( `Success: loaded "${ uri }"`)
				}
			})
	})
}

class Builder {

	constructor( conf ) {
		this.conf = conf
	}

	syncDist() {

		const { sourceDir, distDir } = this.conf
		if ( fs.existsSync(distDir) ) fs.removeSync( distDir )

		fs.mkdirSync( distDir )

		if ( fs.existsSync( `${sourceDir}/assets` ) )
			fs.copySync( `${sourceDir}/assets`, `${distDir}/assets` )

		if ( fs.existsSync( `${sourceDir}/src` ) )
			fs.copySync( `${sourceDir}/src`, `${distDir}/src` )

	}


	rollup(input, { es5 = true } = {}) {
		let babelPresets = [];

		if ( es5 ) {
			babelPresets = [
				[pathByScript(`../node_modules/@babel/preset-env`), {
					targets: { ie: 11 }
				}]
			]
		}

		const plugins = [
			({
				load: async id => {
					if ( /\.sass$/.test(id) ) {
						const code = await sassToJs(id)
						const res = await babel.code(code, {
							presets: [],
							plugins: [
								pathByScript('../node_modules/@babel/plugin-transform-template-literals')
							]
						})
						return res
					}
					return null
				}
			}),
			babelplugin({
				babelrc: false,
				plugins: [
					"@babel/plugin-transform-spread",
					"@babel/plugin-proposal-object-rest-spread",
					"@babel/plugin-transform-react-jsx",
					"@babel/plugin-proposal-class-properties",
					"babel-plugin-transform-remove-console",
				].map( key => pathByScript(`../node_modules/${key}`)),
				presets: babelPresets
			}),
			noderesolver(),
			commonjs(),
			virtual( this[es5 ? 'modulesES5' : 'modulesES6'] ),
		];

		return rollup.rollup({ input, plugins })
	}

	loadExternals() {
		this.modulesES5 = {}
		this.modulesES6 = {}

		if ( this.conf.project.bmp && this.conf.project.bmp.dependencies ) {
			const { dependencies } = this.conf.project.bmp
			const depKeys = Object.keys( dependencies )

			return Promise.all(
				depKeys.map( key => {
					if (this.modulesES5[key] && this.modulesES6[key] )
						return Promise.resolve()

					return new Promise( async resolve => {
						/** FOR LOCAL TESTING
						 *
						 * this.modules[ key ] = fs.readFileSync( dependencies[key].replace('.js', '.es.js') ).toString('utf8')
						 * console.log( this.modules )
						 * resolve()
						 */
						console.log( `Load dependency ${key}: ${dependencies[key]}` )
						const [es5, es6] = await Promise.all([
							download( dependencies[key].replace('.js', '.es6.js') ),
							download( dependencies[key].replace('.js', '.es.js') )
						])
						this.modulesES5[key] = amdToES6( es5 )
						this.modulesES6[key] = es6
						resolve()
					})
				})

			)

		} else {
			console.warn( 'WARNING: Add "bmp" section to your package.json if you use it as dependency' )
		}

	}

	transformHtml(filePath) {
		if ( fs.existsSync(filePath) ) {
			let htmlContent = fs.readFileSync( filePath )
			// TODO make require es6 module dynamic
			return new Promise( resolve => {
				// if ( process.env[ branchEnvKey ] )
				// 	htmlContent = htmlContent.replace( "BRANCH_PATH", `/${process.env[ branchEnvKey ]}/` )

				fs.writeFile( filePath.replace( this.conf.sourceDir, this.conf.distDir ), htmlContent, {}, () => {
					resolve( filePath )
					console.log(`Success: HTML transform ${ filePath } `);
				})
			})
		}

	}

	async run(entrypoint = 'index.js') {
		this.syncDist()

		// load all source to temp folder
		// await this.loadExternals()

		try {

			const [babledBundle, es6Bundle] = await Promise.all([
				this.rollup( `${ this.conf.sourceDir }/${ entrypoint }`, { es5: true } ), // as es5
				this.rollup( `${ this.conf.sourceDir }/${ entrypoint }`, { es5: false } ) // without babel
			])

			/** Write amd version */
			babledBundle.write({
				format: 'amd', // ['amd', 'cjs', 'system', 'es', 'iife' or 'umd']
				exports: 'named',
				file: `${ this.conf.distDir }/${ entrypoint }`
			})

			/** Write es imports version */
			babledBundle.write({
				format: 'es',
				exports: 'named',
				file: `${ this.conf.distDir }/${ entrypoint.replace('.js', '.es6.js') }`
			})

			/** Write es6 version */
			es6Bundle.write({
				format: 'es',
				exports: 'named',
				file: `${ this.conf.distDir }/${ entrypoint.replace('.js', '.es.js') }`
			})
			console.log( 'Success: Rollup', `${ this.conf.distDir }/${ entrypoint.replace('.js', '.es.js') }` )
		} catch (err) {
			log.error(  err.message )
			if (err.frame) log.error(  err.frame )
			if (err.loc) log.warn( `${err.loc.file}:${err.loc.line}:${err.loc.column}` )
			log.error( `${err.plugin}: ${err.code} (${err.hook})` )

			process.exit(1)
			throw new Error()
		}


		const index = `${this.conf.sourceDir}/index.html`
		await this.transformHtml( index )
	}

}

const builder = new Builder({
	sourceDir: path.resolve( 'src' ),
	distDir: path.resolve( 'dist' ),
	project: PKG_JSON
})

const run = async () => {
	await builder.loadExternals()
	const confPath = path.resolve('./build.config.js')
	if (fs.existsSync(confPath)) {
		const { default: config } = await import( confPath )
		if (config.entrypoints) {
			config.entrypoints.forEach(
				entrypoint => builder.run(entrypoint)
			)
		}
	} else {
		builder.run()
	}
}

run()
