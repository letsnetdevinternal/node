import path from 'path';
import { PKG_JSON } from './helpers/path-resolver'
// let bmpCoreModule = 'http://jetsmarter.lo:9100/index.js'
// if ( process.argv.includes( '--env=production' ))
//   bmpCoreModule = 'https://cdn.boomfunc.io/bmp-core/0.0.8/index.js'


let dependencies = {}
if ( PKG_JSON.bmp ) {
	dependencies = PKG_JSON.bmp.dependencies
} else {
	console.warn( 'WARNING: Add "bmp" section to your package.json if you use it as dependency' )
}

const rollupConfig = {
  external: Object.keys( dependencies ),
  output: {
		paths: dependencies,
  },
  format: 'amd', // ['amd', 'cjs', 'system', 'es', 'iife' or 'umd']
}


const sourceDir = path.resolve( 'src' )
const distDir = '/tmp/dist'

export { rollupConfig, sourceDir, distDir }
