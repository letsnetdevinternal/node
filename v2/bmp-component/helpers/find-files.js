const fs = require('fs')
const path = require('path')

/**
 * @param { string } currentDirPath
 * directory where need find files
 * @param { function } callback
 * receives file path and file info
 */
const findFiles = (currentDirPath, callback) => {
  fs.readdir(currentDirPath, function (err, files) {
    if (err) throw new Error(err)
    files.forEach(function (name) {
      let filePath = path.join(currentDirPath, name);
      let stat = fs.statSync(filePath);
      if (stat.isFile()) {
        callback(filePath, stat);
      } else if (stat.isDirectory()) {
        findFiles(filePath, callback);
      }
    });
  });
}

module.exports = { findFiles }
