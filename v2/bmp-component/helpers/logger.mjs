const colors = {
	yellow: '\x1b[33m',
	blue: '\x1b[34m',
	magenta: '\x1b[35m',
	green: '\x1b[32m',
	red: '\x1b[31m',
	reset: '\x1b[0m'
}

// Logger helper
export const log = {
	warn: (...args) => console.log(colors.yellow, ...args, colors.reset),
	error: (...args) => console.log(colors.red, ...args, colors.reset),
	success: (...args) => console.log(colors.green, ...args, colors.reset),
	info: (...args) => console.log(colors.magenta, ...args, colors.reset)
}
