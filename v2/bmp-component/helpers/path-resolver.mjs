
import path from 'path'
import fs from 'fs'

export const pathByRoot = relativePath => {
	const rootPath = process.cwd()
	return path.join( rootPath, relativePath )
}

export const pathByScript = relativePath => path.join(new URL(import.meta.url).pathname, `../../${relativePath}`)
export const resolveModulePath = req => pathByScript(`../node_modules/${req}`)

const PKG_JSON_PATH = pathByRoot('package.json')
const PKG_JSON_STR = fs.readFileSync(PKG_JSON_PATH, 'binary')
export const PKG_JSON = JSON.parse(PKG_JSON_STR)
