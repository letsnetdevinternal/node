import { readFile, writeFileSync } from 'fs';
import { resolve } from 'path';
import uglify from "uglify-js";

const filePath = resolve(`dist/index.js`)
	readFile( filePath, 'utf8', function (err, content) {
		var uglified = uglify.minify(content);
		if ( !err && !uglified.error ) {
			writeFileSync(filePath, uglified.code)
		} else {
			throw (err || uglified.error)
		}
	})

