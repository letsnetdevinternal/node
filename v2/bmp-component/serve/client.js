
const reloadClient = {
	ws: null,
	close( evt ) {
		this.ws = null;
		if ( evt.code == 3001) {
			console.log('RELOAD CLIENT: Server socket was closed (by server)');
		} else {
			console.log('RELOAD CLIENT: Server socket was closed (connection error)');
		}
	},

	incoming( data ) {
		console.log('RELOAD CLIENT: Got from server:', data);
	},
	open() {
		console.log(`RELOAD CLIENT: Client connected to ${port}`);
	},
	error(evt) {
		if ( !this.ws || this.ws.readyState == 1 ) {
			console.log('RELOAD CLIENT: got', evt.type);
		}
	},

	run({ port }) {

		let ws = null
		try {
			let WebSocket = require('ws');
			ws = new WebSocket(`ws://localhost:${port}`)

			ws.onopen = this.incoming
			ws.onmessage = this.incoming
			ws.onclose = this.close
			ws.onerror = this.error
			this.ws = ws

			return ws
		} catch (e) {
			console.log(`Fail to connect to socket on port ${port}`)
			return null
		}
	}
}

module.exports = { reloadClient }
