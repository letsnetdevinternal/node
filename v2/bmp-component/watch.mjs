/** Internal */
import fs from 'fs-extra';
import path from 'path';
import readline from 'readline';

/** Helpers */
import { transformHtml } from './wrappers/html';
import { getExtension } from './helpers/get-extension.mjs';
import { log } from './helpers/logger.mjs';
import DependencyCollection from './helpers/dependency-collection';

/** Wrappers */
import babel from './wrappers/babel';
import { ChokidarInterface, chokidarClassWrapper } from './wrappers/chokidar'
import { sassToJs } from './wrappers/sass-to-js';

/** Configs */
import { sourceDir, distDir } from './config-bundle';
import { PKG_JSON } from './helpers/path-resolver'

const rel = (filepath, loc = {}) => path.relative('.', filepath) + (`${loc.line ? `:${loc.line}:${loc.column}` : ''}`)
// Clear dist folder
if (fs.existsSync(distDir)) {
  fs.removeSync(distDir)
}
fs.mkdirSync(distDir)

// Make a symlink to assets folder
if ( fs.existsSync( `${sourceDir}/assets` ) ) {
	fs.symlink( `${sourceDir}/assets`, `${distDir}/assets` )
}

class Watcher extends ChokidarInterface {

	constructor() {
		super()

		this.throttler = {
			_timeout: null,
			run: fn => {
				clearTimeout( this.throttler._timeout ) // throttled
				this.throttler._timeout = setTimeout( fn, 150 )
			}
		}

		this.queue = []
		this.initStatus = { done: 0, all: 0 }
		this.filesToBundle = new Set()
		this.initialized = false

		if (this.useDependencyCollection) {
			// Initalize dependency collection to know what file is depend to
			this.dependencies = new DependencyCollection({
				esModules: PKG_JSON.bmp && PKG_JSON.bmp.esModules
			})
		}
	}


	/**
	 * compile code and put it to dist folder
	 * @param {string} filePath file that was changed
	 */
	async compile(filePath) {
		const extChangeFile = getExtension(filePath)

		switch (extChangeFile) {
			case 'sass':
				// sass style, compile it by node-sass to "export default `{{css}}`"
				let cssjs = ''
				try {
					cssjs = await sassToJs( filePath )
				} catch (e) {
					throw new Error(e)
				}
				// babel export (cssjs should return "export default")
				const babledCssJs = await babel.code( cssjs )
				// write to javascript file
				fs.outputFileSync( filePath.replace(sourceDir, distDir)+'.js', babledCssJs )
				break
			case 'js':
				// changed file is javascript code
				try {
					// compile it by babel
					const bundle = await babel.file( filePath )
					// write to dist
					fs.outputFileSync( filePath.replace(sourceDir, distDir), bundle )
				} catch (err) {
					// something went wrong with babel compiler or your code
					if ( err instanceof SyntaxError ) {
						log.error(`\nError in ${rel(filePath, err.loc)}`)
						throw new Error(`${err.message.replace(`${filePath}: `, '')}"`)
					} else {
						throw err
					}
				}
				break
			case 'html':
				// html transformer
				// TODO: parse template with conditions/variables
				await transformHtml('index.html')
				break
			default:
				// we can't compile this format
				log.info(`-- Skipped: ${rel(filePath)}`)
		}

		// if ( ws && ws.readyState != require('ws').CLOSED ) ws.send('reload')
	}

	change(sourcePath) {
		if (!fs.existsSync(sourcePath)) {
			this.dependencies.deleteFile(sourcePath)
			return Promise.resolve()
		}
		// relative path to changed file
		log.warn( '-- Requested: ', rel(sourcePath) )

		// add source to queue (throttler)
		this.filesToBundle.add(sourcePath)

		this.throttler.run( () => {
			if (this.useDependencyCollection) { // no disable dependencies flag in cli
				// cache files length
				const initialLength = this.filesToBundle.size
				this.filesToBundle.forEach( filePath => {
					// check for deps in each file
					const deps = this.dependencies.getByFile(filePath)
					// add it to compile queue set
					if (deps.size) deps.forEach( dep => this.filesToBundle.add(dep) )
				})
				if (this.filesToBundle.size > initialLength) {
					// log that we found dependencies
					log.warn(`--- Including ${this.filesToBundle.size - initialLength} dependencies...`)
				}
			}
			const filelist = [...this.filesToBundle]
			// clear files before promise because it will wait for compiler is done
			this.filesToBundle.clear()

			// compile all files in collected array
			Promise
				.all( filelist.map( file => this.compile(file, false) ))
				.then( () => log.success('---- Builded successfully!'))
				.catch( err => {
					log.error(err.message)
					log.error('\n -- Fail to compile.')
				})


		})
	}

	// file add: add it from dependency and build
	add(filePath) {
		if ( this.useDependencyCollection && filePath.endsWith('.js') )
			 // no disable dependencies flag in cli and file is javascript
			 this.dependencies.addFile(filePath)

		if (this.initialized) {
			log.success(`-- Added: ${rel(filePath)}`)
			this.change(filePath)
 		} else {
			this.queue.push({
				path: filePath,
				done: false,
				promise: null
			})
		}
	}

	// file deletion: remove it from dependency and log
	unlink(filepath) {
		if (this.useDependencyCollection)
			this.dependencies.deleteFile(filepath)
		log.info(`-- Deleted: ${rel(filepath)}`)
	}

	// when chokidar is ready to make some changes, first build all project
	ready() {
		console.time('- Completed in')
		// cache all files to bundle length
		this.initStatus.all = this.queue.length
		// for each file we run compiler (this.compile)
		// and map it to object { path, done, promise }
		const changesPromiseList = this.queue
			.map( change => ({
				...change,
				promise: this.compile(change.path, false)
			})).map( async change => {
				// log console when queue is updated
				try {
					await change.promise
				} catch (e) {
					log.error(e.message)
				}
				readline.clearLine(process.stdout, 1);
				readline.cursorTo(process.stdout, 1, null);
				process.stdout.write( `- Progress... ${++this.initStatus.done}/${this.initStatus.all} files is builded.\r` )
			})

		Promise.all( changesPromiseList )
			.then( () => {
				this.initialized = true
				log.success('\t\n-- Initial scan complete. Ready for changes')
				console.timeEnd("- Completed in")
			})
	}


}

chokidarClassWrapper(Watcher)
