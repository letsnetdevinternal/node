
import babel from '@babel/core';
import { pathByScript } from '../helpers/path-resolver.mjs';

const options = {
	comments: false,
	babelrc: false,
	plugins: [
		"@babel/plugin-transform-spread",
		"@babel/plugin-proposal-object-rest-spread",
		"@babel/plugin-transform-react-jsx",
		"@babel/plugin-proposal-class-properties",
	].map( key => pathByScript(`../node_modules/${key}`) ),
	presets: [
		[ pathByScript(`../node_modules/@babel/preset-env`),  {
			modules: 'amd',
			targets: ["ie >= 11"]
		}]
	]

}

const file = (filepath, conf = {}) => {
	return new Promise( (resolve, reject) => {
		babel.transformFile( filepath , {
			...options,
			...conf
		}, (err, res) => {
			if ( err ) {
				reject( err )
			} else {
				// console.log( '- Babel completed: ', filepath );
				resolve( res.code );
			}
		});
	});
}


const code = async (code, conf = {}) => {
	const result = await babel.transform( code , {
		...options,
		presets: conf.presets ? conf.presets : options.presets,
		plugins: conf.plugins ? conf.plugins : options.plugins
	})
	return result.code
}

export default { file, code }
