
import chokidar from 'chokidar'
import path from 'path'
import { sourceDir } from '../config-bundle'

export class ChokidarInterface {

	constructor() {
		this.useDependencyCollection = !process.argv.find( arg => arg === '--dependencies=false' )
	}

	change() { throw new Error('"change" method is not specified. ChokidarInterface') }
	add() { throw new Error('"add" method is not specified. ChokidarInterface') }
	unlink() { throw new Error('"unlink" method is not specified. ChokidarInterface') }
	ready() { throw new Error('"ready" method is not specified. ChokidarInterface') }

}

export const chokidarClassWrapper = Constructor => {
	/** @var { ChokidarInterface } instance */
	const instance = new Constructor()

	// Initalize watch instance
	let watcher = chokidar.watch(sourceDir, {
		persistent: true,
		usePolling: true,
		ignored: `${path.resolve('src/assets/**')}|**.txt|**.ico|.DS_Store`,
		awaitWriteFinish: {
			stabilityThreshold: 100,
			pollInterval: 100
		}
	})

	watcher
		.on('change', filepath => instance.change(filepath) )
		.on('add', filepath => instance.add(filepath) )
		.on('unlink', filepath => instance.unlink(filepath) )
		.on('ready', () => instance.ready() )

	return watcher
}
