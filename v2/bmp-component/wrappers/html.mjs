import { sourceDir, distDir } from '../config-bundle'
import fs from 'fs'

export const transformHtml = ( filePath, dist = distDir ) => {
  let htmlContent = fs.readFileSync( `${ sourceDir }/${ filePath  }`)
  // TODO make require es6 module dynamic
  return new Promise( resolve => {
    fs.writeFile(`${ dist }/${ filePath }`, htmlContent, {}, () => {
      resolve( filePath )
    })
  })
}

