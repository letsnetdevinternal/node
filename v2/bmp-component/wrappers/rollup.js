const rollup = require('rollup')
const babel = require('rollup-plugin-babel')
const commonjs = require('rollup-plugin-commonjs')


const rollupFile = (path) => {
  return rollup.rollup({
    input: path,
    output: {
      globals: {
        BmpApp: 'BmpApp',
        BMPVD: 'BMPVD',
        BMPVDWebComponent: 'BMPVDWebComponent',
        observe: 'observe'
      },
    },
    plugins: [
			commonjs({
				include: 'node_modules/**',
			}),
      babel({
				exclude: "node_modules/**",
				babelrc: false,
				plugins: [
					"transform-es2015-spread",
					"external-helpers",
					"syntax-jsx",
					"transform-react-jsx",
					"transform-object-rest-spread"
				].map( key => require( `babel-plugin-${key}` ) )
      }),
    ]
  })
}

module.exports = { rollupFile }
