
import nodesass from 'node-sass'

const sassToJs = (filepath, options) => {
	return new Promise( (resolve,reject) => {
		nodesass.render({
			file: filepath,
			...options
		}, async (err, result) => {
			if (err) {
				reject(err.formatted)
			} else {
				result.css ? (
					resolve( 'export default `'+result.css+'`' )
				) : (
					reject(result)
				)
			}
		});
	})
}

export { sassToJs }
