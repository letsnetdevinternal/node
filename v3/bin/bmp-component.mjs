#!/bin/sh
":" //# comment; exec /usr/bin/env node --experimental-modules --no-warnings "$0" "$@"

/** Core helpers */
import path from 'path'
import fs from 'fs'
import util from 'util'

/** Node modules dependencies */
import commandLineArgs from 'command-line-args'
import getUsage from 'command-line-usage'

/** Local helpers */
import { log, logmod } from '../helpers/logger.mjs'
import { pathByScript } from '../helpers/path-resolver.mjs';

// Available definition of commands and options
import definitions from './usage.mjs'
import { PKG_JSON } from '../helpers/path-resolver.mjs'

const fatal = (...errs) => { log.error(...errs);process.exit(1) }

// Firstly, get command
// that user should specify as first parameter
const { command, _unknown } = commandLineArgs([definitions.main], { stopAtFirstUnknown: true })
const cmdDefs = definitions.main.commands.find( cmd => cmd.name === command )
const optionList = [
	...definitions.options,
	...(cmdDefs ? cmdDefs.optionList : [])
]
// Start main processing
log.info('\nBoompack component cli tool v1.0')

// That parse options command line arguments like --entrypoint
let options = {}
try {
	options = commandLineArgs(optionList, { argv: _unknown || [] } )
} catch (e) {
	if (e.name === 'UNKNOWN_OPTION') {
		fatal(
			`\nUnknown option: ${logmod.bgwhite(e.optionName)}\n`,
			logmod.yellow(`\nNote:`, `Use "bmp-component --help" option to display usage guide.\n`)
		)
	} else {
		throw e
	}
}

// check for help command and display usage
if ( options.help ) {
	// Usage helper
	const usage = getUsage([
		{ content: 'Use for build and watch src' },
		{ header: 'Usage', content: `$ bmp-component ${command || '<command>'} <options>` },
		{
			header: `Command${command ? '': ' List'}`,
			content: definitions.main.commands
				.filter( cmd => !command || cmd.name === command )
				.map( cmd =>
					getUsage( [{ header: cmd.name, optionList: cmd.optionList }] )
				)
		},
		{ header: 'Options', optionList: definitions.options }
	])

	process.stdout.write( usage )
	process.exit( 0 )
}
if (options.version) {
	process.stdout.write('\nv1.0\n')
	process.exit( 0 )
}

// Check for command is in command list
const commandNames = definitions.main.commands.map( cmd => cmd.name )
if ( !commandNames.includes(command) ) {
	// Command not expected: we haven't suck task in ../task folder
	fatal(
		`\nUnknown command ${logmod.bgwhite(command)}\n\n`,
		'Possible commands:\n',
		logmod.green('- '+ commandNames.join('\n - ')) + '\n',
		logmod.yellow(`\nNote:`, `Use "bmp-component --help" option to display usage guide.\n`)
	)
}


// Check for required parameters
optionList.forEach( opt => {
	if (opt.required && !options[opt.name]) {
		// option is required but not passed as cli argument
		fatal(`Missing requred parameter\n`, getUsage([{ header: command, optionList: [opt] }]))
	}
})

const taskModulePath = pathByScript(`./task/${command}.mjs`)
console.log(taskModulePath)
// Check for task executable file
if (!fs.existsSync(taskModulePath)) {
	throw new Error(`Module in "${taskModulePath}" not found`)
}

// Main run function
const BmpComponent = async options => {
	// resolve config of task
	const conf = {
		sourceDir: path.resolve( options.src || './src' ),
		distDir: path.resolve( options.dist || './dist' ),
		project: PKG_JSON,
		...options
	}
	// Task should be available in ../task folder
	// and must have .mjs format (es module)
	const { default: Task } = await import( taskModulePath )
	// run task and pass resolved conf
	log.warn(`\nRun task "${command}" with config:`)
	console.log(`Source directory: ${logmod.yellow(conf.sourceDir)}`)
	console.log(`Destination directory: ${logmod.yellow(conf.distDir)}\n`)
	await Task.run(conf)
	log.success('Job successfully completed\n')
}

process.on('unhandledRejection', (...args) => {
	fatal( util.format.apply(console, args) )
	process.exit(1)
});

BmpComponent(options)
