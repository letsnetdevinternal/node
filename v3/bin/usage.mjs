export default {
	// command, like "build", "watch"
	main: {
		name: 'command',
		defaultOption: true,
		description: 'Specify task that should be executed',
		commands: [
			// Available list of commands.
			{
				name: 'build',
				summary: 'Build your application',
				optionList: [
					{ name: 'entrypoint', alias: 'e', type: String, multiple: true, description: 'Entrypoints file list' },
					{ name: 'src', alias: 's', type: String, description: 'Source folder path' },
					{ name: 'dist', alias: 'd', type: String, description: 'Where to put bundled project' },
				]
			},
			{
				name: 'set-env',
				summary: 'Update env. Use it after build',
				optionList: [
					{ name: 'entrypoint', alias: 'e', type: String, required: true, description: 'Entrypoints files list' },
					{ name: 'output', alias: 'o', type: String, required: true, description: 'required\tfile path to output code' },
					{ name: 'key', alias: 'k', type: String, required: true, description: 'required\tenviroment key' },
				]
			},
			{
				name: 'watch',
				summary: 'Build application and than rebuild when file changed.',
				optionList: [
					{ name: 'src', alias: 's', type: String, multiple: true, description: 'Source folder path' },
				]
			},
			{
				name: 'link',
				summary: 'Link local reference packages, uses "ref" section in package.json',
				optionList: [
					{ name: 'dist', alias: 'd', type: String, description: 'Where to put bundled project' },
				]
			},
		]
	},
	// options with -- prefix, like "--help", "--src=./source-code/"
	options: [
		{ name: 'help', alias: 'h', type: Boolean, description: 'Display the usage guide.' },
		{ name: 'version', alias: 'v', type: Boolean, description: 'Print the version' },
	]
}
