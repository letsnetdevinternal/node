import fs from "fs"
import readline from 'readline'
import path from 'path'

class RequireDriver {

	hasDependency(string) {
		return /require\(.*\)$/.test( string.trim() )
	}

	extractDependency(string) {
		const matches = /require\(\s?['"](.*?)['"]\)/.exec(string)
		return matches[1]
	}
}

class ESModulesDriver {

	hasDependency(string) {
		return  /from\s+(['"](.*?)['"])/.test(string) ||
			string.startsWith('import \'') || string.startsWith('import "')
	}

	extractDependency(string) {
		let matches = /from\s+['"](.*?)['"]/.exec( string.trim() )
		if (!matches) matches = /import\s+['"](.*?)["']/.exec( string.trim() )
		return matches[1]
	}

}

export default class DependencyCollection {

	/**
	 *
	 * @param { Array } fileList preset file list of collection
	 * @param { Boolean } esModules true = search for import{} syntax, false = require()
	 */
	constructor({ fileList = [], esModules = true } = {}) {
		this.collection = {}
		this.driver = esModules ? new ESModulesDriver() : new RequireDriver()
		fileList.forEach( filepath => this.addFile(filepath) )
	}

	parseFileLine(filePath, line) {
		const parent = path.parse(filePath)
		const resolvedpath = path.resolve(filePath)

		if ( this.driver.hasDependency(line) ) {
			const dependency = this.driver.extractDependency(line)
			const absPath = path.resolve( parent.dir, dependency )

			if ( fs.existsSync(absPath) ) {
				if ( !Array.isArray(this.collection[absPath]) )
					this.collection[ absPath ] = []

				this.collection[ absPath ].push( resolvedpath )
			}
		}
	}

	addFile(filePath) {
		return new Promise( (resolve, reject) => {
			const fileReader = readline.createInterface({
				input: fs.createReadStream( filePath )
			})

			// one dependency can be in 1 line
			fileReader.on('line', line => this.parseFileLine(filePath, line) )
			fileReader.on('error', reject)
			fileReader.on('close', resolve )
		})
	}

	deleteFile(filePath) {
		if (this.collection[filePath])
			delete this.collection[filePath]
		Object.keys(this.collection).forEach(entry => {
			this.collection[entry] = this.collection[entry].filter(dependency => {
				return dependency !== filePath
			})
		})
	}

	getDependencyMap(entry) {
		if (!this.collection[entry] || this.passed.includes(entry))
			return []
		this.passed.push(entry)
		return this.collection[entry].reduce( (res, filepath) =>  [
			...res, filepath, ...this.getDependencyMap(filepath)
		], [])
	}


	getByFile(filepath) {
		this.passed = []
		return new Set(
			this.getDependencyMap( path.resolve(filepath) )
		)
	}

}

