
import request from 'request'
import { log } from './logger.mjs';


const download = uri => {
	return new Promise( resolve => {
		request
			.get({ uri }, (err, res, data) => {
				if ( err ) {
					log.error( `Fail: cannot get "${ uri }",`,err )
				} else {
					resolve( data )
					log.success( `Success: loaded "${ uri }"`)
				}
			})
	})
}

export { download }
