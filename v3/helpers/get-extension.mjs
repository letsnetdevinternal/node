import path from 'path'

let getExtension = (filename) => {
  let ext = path.extname(filename || '').split('.')
  return ext[ext.length - 1]
}

export { getExtension }
