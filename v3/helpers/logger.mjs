import util from 'util'

const unify = (...args) => util.format.apply(console, ...args)
const colorReset = '\x1b[0m'
export const logmod = {
	default: (...args) => colorReset + unify(args),
	yellow: (...args) => `\x1b[33m${ unify(args) }${colorReset}`,
	blue:  (...args) => `\x1b[34m${ unify(args) }${colorReset}`,
	magenta:  (...args) => `\x1b[35m${ unify(args) }${colorReset}`,
	green: (...args) => `\x1b[32m${ unify(args) }${colorReset}`,
	red: (...args) => `\x1b[31m${ unify(args) }${colorReset}`,
	bgwhite: (...args) => `\x1b[47m${ unify(args) }${colorReset}`,
}

// Logger helper
export const log = {
	write: (...args) => console.log(...args),
	warn: (...args) => {
		if (!args.length) console.log((new Error().stack))
		console.log( logmod.yellow(...args) )
	},
	error: (...args) => console.log( logmod.red(...args) ),
	success: (...args) => console.log( logmod.green(...args) ),
	info: (...args) => console.log( logmod.magenta(...args) )
}
