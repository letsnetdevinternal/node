const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 5801 })
let clientsArr = []

wss.on('connection', function connection(ws) {
  clientsArr.push(ws)
  ws.on('message', function incoming(message) {
    if (message == 'reload') {
      for (const client of clientsArr) {
        try {
          client.send(message)
        } catch (err) {
          clientsArr = clientsArr.filter(
						closeClient => closeClient != client
					)
        }
      }
    }
  });
  ws.send('reload')
});
