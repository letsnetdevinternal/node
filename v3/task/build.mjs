
/** Core */
import path from  'path'
import fs from  'fs-extra'
import Task from '../core/interface/task.mjs';

/** Rollup dependencies */
import rollup from 'rollup'
import babelplugin from 'rollup-plugin-babel'
import noderesolver from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs'

/** Helpers */
import { pathByScript, pathByRoot } from '../helpers/path-resolver.mjs'
import { log } from '../helpers/logger.mjs';
import Link from './link.mjs'

/** Wrappers */
import { sassToJs } from '../wrappers/sass-to-js.mjs'
import babel from '../wrappers/babel.mjs'


class Builder extends Task {

	static async run(conf) {
		const builder = new Builder(conf)

		builder.syncDist()
		// Link.run(conf)
		const confPath = path.resolve('./build.config.js')
		fs.copySync('package.json', `${conf.distDir}/package.json`)
		if (fs.existsSync(confPath)) {
			const { default: config } = await import( confPath )
			if (config.entrypoints) {
				await Promise.all(
					config.entrypoints.map( entrypoint => builder.bundle(entrypoint) )
				)
			}
		} else {
			await builder.bundle({
				filepath: './index.js',
				versions: {
					amd: 'index.js',
					esm: 'index.es6.js'
				}
			})
		}
		return builder
	}

	constructor( conf ) {
		super()
		this.conf = conf
	}

	syncDist() {

		const { sourceDir, distDir } = this.conf
		if ( fs.existsSync(distDir) ) fs.removeSync( distDir )

		fs.mkdirSync( distDir )

		if ( fs.existsSync( `${sourceDir}/assets` ) )
			fs.copySync( `${sourceDir}/assets`, `${distDir}/assets` )

		if ( fs.existsSync( `${sourceDir}/src` ) )
			fs.copySync( `${sourceDir}/src`, `${distDir}/src` )

	}

	sassPlugin() {
		return {
			load: async id => {
				if ( !/\.sass$/.test(id) ) return null
				const code = await sassToJs(id, { outputStyle: this.conf.minify ? 'compressed' : '' })
				return await babel.code(code, {
					presets: [],
					plugins: [ pathByScript('./node_modules/@babel/plugin-transform-template-literals') ]
				})
			}
		}
	}

	/**
	 * Resolve package by "refs" section in package.json
	 * set link to main file of specified package path
	 */
	refsPlugin() {
		return {
			resolveId: id => {
				if (this.conf.project.refs && this.conf.project.refs[id]) {
					const packagePath = pathByRoot(this.conf.project.refs[id])
					const refPackageJson = JSON.parse( fs.readFileSync(`${packagePath}/package.json`))
					if (!refPackageJson.main) {
						throw new Error(`Please specify "main" section in ${packagePath}/package.json`)
					}
					const mainFilePath = path.resolve(`${packagePath}/${refPackageJson.main}`)
					if (!fs.existsSync(mainFilePath)) {
						throw new Error(`File doesn't exists: ${mainFilePath}`)
					}
					return path.resolve(`${packagePath}/${refPackageJson.main}`)
				}
				return null
			}
		}
	}

	rollup(input, { presets = [], es5 = false } = {}) {
		const plugins = [
			this.sassPlugin(),
			this.refsPlugin(),
			babelplugin({
				babelrc: false,
				plugins: [
					["@babel/plugin-transform-spread"],
					["@babel/plugin-proposal-object-rest-spread"],
					["@babel/plugin-transform-react-jsx", {
						pragma: "BMPVD.createBMPVirtulaDOMElement"
					}],
					["@babel/plugin-proposal-class-properties"],
					["babel-plugin-transform-remove-console"]
				].map( ([key, ...args]) => [pathByScript(`./node_modules/${key}`), ...args] ),
				presets
			}),
			noderesolver(),
			commonjs()
		];

		return rollup.rollup({
			input,
			plugins,
			onwarn: () => {}
		})
	}

	getBundlePath(amdPath, defaultPath) {
		let distPath = this.conf.distDir
		if (typeof amdPath === 'object')
			distPath += `/${amdPath.export}`
		else if (typeof amdPath === 'string')
			distPath += `/${amdPath}`
		else
			distPath += `/${defaultPath}`
		return path.resolve(distPath)
	}

	async bundle({ filepath, versions } = {}) {
		if (!filepath)
			throw new Error('filepath of bundle is not specified')
		const ext = path.extname(filepath)
		const { sourceDir, distDir } = this.conf
		if (ext === '.js') {
			try {
				const [es5Bundle, esmBundle] = await Promise.all([
					this.rollup( `${ sourceDir }/${ filepath }`, {
						es5: true,
						presets: [
							[pathByScript(`./node_modules/@babel/preset-env`), { targets: { ie: 11 } }]
						]
					}), // with babel
					this.rollup( `${ sourceDir }/${ filepath }` ), // without babel
				])

				/** Write amd version */
				const promises = [
					// by default we generates es6 module
					es5Bundle.write({
						format: 'es',
						exports: 'named',
						file: this.getBundlePath(versions.esm, filepath).replace(/\.es\.js$/, '.es6.js')
					})
				]

				const bundlePathAmd = this.getBundlePath(versions.amd, filepath)
				if (versions.amd) {
					/** Write as amd module */
					promises.push(
						es5Bundle.write({
							format: 'amd', // ['amd', 'cjs', 'system', 'es', 'iife' or 'umd']
							exports: 'named',
							file: bundlePathAmd
						})
					)
				}

				if (versions.esm) {
					/** Write as esm module */
					promises.push(
						esmBundle.write({
							format: 'es',
							exports: 'named',
							file: this.getBundlePath(versions.esm, filepath)
						})
					)
				}
				await Promise.all(promises)
				log.write( 'Success: Rollup', filepath )
			} catch (err) {
				log.error(  err )
				process.exit(1)
				throw new Error(err)
			}
		} else {
			const absFilepath = path.resolve(`${sourceDir}/${filepath}`)
			fs.copySync(absFilepath, absFilepath.replace(sourceDir, distDir))
		}
	}

}

export default Builder
