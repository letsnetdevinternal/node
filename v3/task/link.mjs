
import fs from 'fs-extra'
import { log } from '../helpers/logger.mjs'
import { pathByRoot } from '../helpers/path-resolver.mjs'
import Task from '../core/interface/task.mjs'

export default class Link extends Task {

	static run(conf) {
		const instance = new Link(conf)
		return instance.create(conf.dir)
	}

	constructor(conf) {
		super()
		this.conf = conf
	}

	getLinksMap() {
		if (!this.conf.project.refs) {
			log.warn('No "refs" section specified in package.json');
			return null
		}
		return this.conf.project.refs
	}

	clearFolder(dest) {
		if (fs.existsSync(dest)) {
		 fs.removeSync(dest)
		}
		fs.mkdirpSync(dest)
	}

	create() {
		const links = this.getLinksMap()
		const linkFolder = pathByRoot(`node_modules`)
		if (links) {
			this.clearFolder(linkFolder)

			const errors = Object.keys(links).map(dependency => {
				const packageDir = pathByRoot(links[dependency])
				 if ( !fs.existsSync(packageDir) ) {
					 return packageDir
				 }
				 fs.symlinkSync( packageDir, `${linkFolder}/${dependency}` )
				 return null
			}).filter(err => err != null)
			if (!errors.length) {
				log.warn('Written links:', JSON.stringify(links, null, 2))
			} else {
				log.error( ...errors.map(err => `Package not found: "${err}": \n`) )
			}
		}
	}

}
