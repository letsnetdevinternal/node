
import fs from 'fs'
import { log } from '../helpers/logger.mjs'
import { pathByScript, pathByRoot, PKG_JSON } from '../helpers/path-resolver.mjs'

import babel from '@babel/core'
import dotenv from 'dotenv'
import Task from '../core/interface/task.mjs';

export default class SetEnv extends Task {

	static run(conf) {
		const instance = new SetEnv(conf)
		return instance.update(conf.key)
	}

	constructor(conf) {
		super()
		this.conf = conf
	}

	async update(env) {
		const bundlePath = pathByRoot(this.conf.entrypoint)
		if (!fs.existsSync(bundlePath)) {
			throw new Error(`File not exists ${this.conf.entrypoint}`)
		}

		/** Try to parse ENV variables into process.env */
		const envPath = `${process.cwd()}/env/.env.${env}`
		if (!fs.existsSync(envPath)) {
			throw new Error(`ENV not exists: "/env/.env.${env}"`)
		}
		const { parsed: projEnv } = dotenv.config({ path: `${process.cwd()}/env/.env.${env}` })
		log.write( "Found env:" )
		log.warn(JSON.stringify(projEnv, null, 2))

		/** Replace process.env in bundle */
		const { code } = babel.transformSync(
			fs.readFileSync(bundlePath).toString('UTF-8'),
			{
				babelrc: false,
				presets: [],
				plugins: [
					[ pathByScript(`./node_modules/babel-plugin-transform-inline-environment-variables`) ],
				]
			}
		)
		const outputPath = pathByRoot(this.conf.output)
		fs.writeFileSync( outputPath, code )


		log.success(`Written "${env}" to "${this.conf.output}"`)

	}

}
