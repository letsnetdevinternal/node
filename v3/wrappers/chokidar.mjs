
import chokidar from 'chokidar'
import path from 'path'
import fs from 'fs-extra'
import Link from '../task/link.mjs';
import { pathByRoot } from '../helpers/path-resolver.mjs';

export class ChokidarInterface {

	constructor() {
		this.useDependencyCollection = !process.argv.find( arg => arg === '--dependencies=false' )
	}

	__undefinedMethod() {
		throw new Error(`"${method}" method is not specified. ChokidarInterface`)
	}

	change() { this.__undefinedMethod('change') }
	add		() { this.__undefinedMethod('add') 		}
	unlink() { this.__undefinedMethod('unlink') }
	ready	() { this.__undefinedMethod('ready')	}

}

export const chokidarClassWrapper = Constructor => {

	return {
		run: conf => {
			return new Promise( resolve => {
				// Make a symlink to assets folder
				// Clear dist folder
				if (fs.existsSync(conf.distDir)) {
					fs.removeSync(conf.distDir)
				}
				fs.mkdirSync(conf.distDir)
				Link.run(conf)
				fs.symlinkSync( pathByRoot(`node_modules`), pathByRoot(`dist/node_modules`) )
				if ( fs.existsSync( `${conf.distDir}/assets` )  ) {
					fs.removeSync( `${conf.distDir}/assets` )
				}
				if ( fs.existsSync( `${conf.sourceDir}/assets` ) ) {
					fs.symlink( `${conf.sourceDir}/assets`, `${conf.distDir}/assets` )
				}


				// Initalize watch instance
				let watcher = chokidar.watch(conf.sourceDir, {
					persistent: true,
					usePolling: true,
					ignored: `${path.resolve(`${conf.sourceDir}/assets/**`)}|**.txt|**.ico|.DS_Store`,
					awaitWriteFinish: {
						stabilityThreshold: 100,
						pollInterval: 100
					},
				})

				/** @var { ChokidarInterface } instance */
				const instance = new Constructor(conf)
				watcher
					.on('change', filepath => instance.change(filepath) )
					.on('add', filepath => instance.add(filepath) )
					.on('unlink', filepath => instance.unlink(filepath) )
					.on('ready', () => instance.ready() )

				instance.setEnv('.env.local')
				return watcher
			})
		}
	}
}
